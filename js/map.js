/**
 * Map class for rendering a map image on a canvas, with bounding boxes.
 *
 * There are two key terms used here:
 * - Box: Coordinates for a rectangle with two (start_x, start_y), (end_x, end_y) coordinate pairs.
 * - Bounds: A rectangle defined using one coordinate pair (x,y) as well as a height and width measurement.
 *
 * Boxes are used when drawing interactively on the map, because that's how the coordinate pairs are passed from the mouse events.
 * Everything is stored in the DB as Bounds, though. Bounds provide the height and width measurements which are required by the canvas rectangle drawing functions. 
 *
 * All coordinates are stored as percentages of the canvas (image). 
 * That way we can easily scale the map image without having to re-work all of the bounding coordinates.
 * The getScaled_ methods return coordinates which have been scaled from their percentage values to pixel values for the current canvas dimensions.
 **/

function Map(canvas) {
  this.canvas = canvas;
  this.start_x = -1;
  this.start_y = -1;
  this.end_x = -1;
  this.end_y = -1;

  this.bounds = [];
  this.ratio = null;
}

/**
 * Set the background image for the canvas to the image pointed to by img_url.
 */
Map.prototype.updateBase = function(img_url, width, height) {
  this.canvas.css('background-image', 'url(' + img_url + ')');
  this.canvas.css('background-size', '100%, 100%');
  this.canvas.css('background-repeat', 'no-repeat');
  this.canvas.attr('width', width);
  this.canvas.attr('height', height);
  this.setRatio(width, height);
}

/**
 * Get the canvas element.
 *
 * This is the DOM element, not a jQuery wrapped element.
 */
Map.prototype.getCanvas = function() {
  return this.canvas.get(0);
}

/**
 * Get the current height of the canvas element in pixels.
 */
Map.prototype.getHeight = function() {
  return jQuery(this.canvas).height();
}

/**
 * Get the current width of the canvas element in pixels.
 */
Map.prototype.getWidth = function() {
  return jQuery(this.canvas).width();
}

/**
 * The setStartX, setStartY, setEndX, setEndY, as well as the Box functions are used when interactively 'drawing' the bounding boxes in the admin UI.
 *
 * Set all four values and then call drawBox() to draw the corresponding box on the canvas.
 */

/** 
 * Set the horizontal component of the starting coordinate of the box.
 */
Map.prototype.setStartX = function (val) {
  var width = this.getWidth();
  this.start_x = val / width;
}

/** 
 * Set the vertical component of the starting coordinate of the box.
 */
Map.prototype.setStartY = function (val) {
  var height = this.getHeight();
  this.start_y = val / height;
}

/** 
 * Set the horizontal component of the ending coordinate of the box.
 */
Map.prototype.setEndX = function (val) {
  var width = this.getWidth();
  this.end_x = val / width;
}

/** 
 * Set the vertical component of the ending coordinate of the box.
 */
Map.prototype.setEndY = function (val) {
  var height = this.getHeight();
  this.end_y = val / height;
}

/**
 * Fetch a box object representing the current box drawn on the canvas.
 */
Map.prototype.getBox = function () {
  var box = {};
  box['start_x'] = this.start_x;
  box['start_y'] = this.start_y;
  box['end_x'] = this.end_x;
  box['end_y'] = this.end_y;
   
  return box;
}

/**
 * Get a box, with dimensions scaled to fit the current height and width of the canvas.
 */
Map.prototype.getScaledBox = function () {
  var box = this.getBox();
  var width = this.getWidth();
  var height = this.getHeight();

  box['start_x'] = box['start_x'] * width;
  box['start_y'] = box['start_y'] * height;
  box['end_x'] = box['end_x'] * width;
  box['end_y'] = box['end_y'] * height;
  box['width'] = box['width'] * width;
  box['height'] = box['height'] * height;

  return box;
}

/**
 * Get a 2D drawing context for the canvas.
 */
Map.prototype.getContext = function() {
  var c = this.getCanvas();
  return c.getContext('2d');
}

/**
 * Clear the canvas of all drawn shapes.
 */
Map.prototype.clear = function() {
  ctx = this.getContext();
  width = jQuery(this.canvas).width();
  height = jQuery(this.canvas).height();
  ctx.clearRect(0,0,width, height); 
}

/**
 * Once both start and end coordinate pairs have been set, draw the box.
 */
Map.prototype.drawBox = function() {

  box = this.getScaledBox();

  var width = box.end_x - box.start_x;
  var height = box.end_y - box.start_y;

  this.drawRectangle(box['start_x'], box['start_y'], width, height);  
}

/**
 * Given a BOUND, draw it on the canvas.
 */
Map.prototype.drawBound = function(bound) {
  this.drawRectangle(bound['x'], bound['y'], bound['width'], bound['height']);
}

/**
 * Loop through a collection of bounds and draw each one.
 */
Map.prototype.drawBounds = function () {
  var bounds = this.getScaledBounds();
  bounds.forEach(function (bound, index, array) {
    this.drawBound(bound);
  }, this);
}

/**
 * Draw a rectangle on the canvas.
 */
Map.prototype.drawRectangle = function(x, y, width, height) {
  var box_colour = Drupal.settings.ug_whereisit.box_colour;
  var line_width = Drupal.settings.ug_whereisit.line_width;
  var fill_transparency = Drupal.settings.ug_whereisit.fill_transparency;

  ctx = this.getContext();
  ctx.strokeStyle = "#" + box_colour;
  ctx.lineWidth = line_width; 
  ctx.strokeRect(x, y, width, height);
  ctx.globalAlpha = fill_transparency;
  ctx.fillStyle = "#" + box_colour;
  ctx.fillRect(x, y, width, height);
}

/**
 * Get all the registered bounding boxes for this map.
 */
Map.prototype.getBounds = function () {
  //This JSON business is a hack to get a deep copy of the bounds array.
  return JSON.parse(JSON.stringify(this.bounds));
}

/**
 * Register a bounding box with the map.
 */
Map.prototype.storeBound = function (bound) {
  this.bounds.push(bound);
}

/**
 * Get all registered bounding boxes, scaled to fit the canvas.
 */
Map.prototype.getScaledBounds = function () {
  var bounds = this.getBounds();
  
  var width = this.getWidth();
  var height = this.getHeight();

  bounds.forEach(function (bound, index, array) {

    bound['x'] *= width;
    bound['y'] *= height;
    bound['width'] *= width;
    bound['height'] *= height;
  }, this);

  return bounds;
}

/**
 * Given a floor code, fetch a base map image URL and update the base image of the canvas element.
 *
 * floor_code - The code representing the floor image to load. 
 *
 * fill - Optional boolean parameter, defaults to false. 
 *        If true, the map will expand to fill its container after loading the new base layer.
 *        This parameter was added in order to allow the map to expand to fill its container on the public, responsive interface, 
 *        while still retaining the original dimensions on the administration interface.  
 */
Map.prototype.updateMap = function (floor_code, fill) {
  //Optional parameter, defaults to false.  
  fill = fill || false;

  jQuery.ajax({
    context: this,
    url: "/where/map/" + floor_code,
  })
  .done(function(data) {
    data = JSON.parse(data);
    this.updateBase(data['url'], data['width'], data['height']);
    if (fill) {
      this.fillContainer();
    }
    this.drawBounds();
  });
}

/**
 * Canvas elements won't resize on their own, so we've got to add the behaviour in with JS.
 */
Map.prototype.fillContainer = function() {
  var container = jQuery(this.canvas).parent();
  var container_width = jQuery(container).width();
  var scaled_height = container_width / this.ratio; 

  this.canvas.attr('width', container_width);
  this.canvas.attr('height', scaled_height);
}

/**
 * If the aspect ratio hasn't been set yet, set it.
 *
 * Passing a TRUE value will force the aspect ratio to be set, even if it already exists. 
 */
Map.prototype.setRatio = function(width, height) {
  var width = this.getWidth();
  var height = this.getHeight();

  this.ratio = width / height;
}

/**
 * If the aspect ratio hasn't been set yet, set it.
 *
 * Passing a TRUE value will force the aspect ratio to be set, even if it already exists. 
 */
Map.prototype.detectRatio = function(reset) {
  //Hacky implementation of an optional function parameter.
  reset = reset || false;

  if(this.ratio == null || reset == true) {
    var width = this.getWidth();
    var height = this.getHeight();

    this.ratio = width / height;
  }
}
