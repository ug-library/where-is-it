(function ($) {
  $(document).ready(function() {
    
    $('#where-named select').change(function(event) {
      var val = $(this).val();
      var url = "/where/areas/area/" + val;
      window.location.href = url;
    });

    where_submit_redirect = function(event) {
      event.preventDefault();

      var val = $('select', $(this).parent()).val();
      var url = "/where/areas/area/" + val;
      window.location.href = url;
    }

    $('#where-room-submit').click(where_submit_redirect);
    $('.where-floor-submit').click(where_submit_redirect);
     

  }); 
}(jQuery));
