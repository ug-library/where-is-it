<?php
/**
 * @file
 * where_is_it_views.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function where_is_it_views_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'areas';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'ug_whereisit_areas';
  $view->human_name = 'Areas';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Call Number Areas';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer where is it';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'mini';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'id' => 'id',
    'label' => 'label',
    'floor' => 'floor',
    'code' => 'code',
    'nothing' => 'nothing',
  );
  $handler->display->display_options['style_options']['default'] = 'label';
  $handler->display->display_options['style_options']['info'] = array(
    'id' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'label' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'floor' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'code' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['label'] = 'Area Links';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<ul class="action-links"><li><a href="/admin/config/whereisit/areas/add?destination=admin/config/whereisit/named-areas">Add area</a></li>
<li><a href="/admin/config/whereisit/areas/import?destination=admin/config/whereisit/named-areas">Import area</a></li>
</ul>';
  $handler->display->display_options['header']['area']['format'] = 'filtered_html';
  /* Field: Area: Internal, numeric area ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'ug_whereisit_areas';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['exclude'] = TRUE;
  /* Field: Area: Label */
  $handler->display->display_options['fields']['label']['id'] = 'label';
  $handler->display->display_options['fields']['label']['table'] = 'ug_whereisit_areas';
  $handler->display->display_options['fields']['label']['field'] = 'label';
  $handler->display->display_options['fields']['label']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['label']['alter']['path'] = '/admin/config/whereisit/areas/area/[id]';
  /* Field: Area: Floor */
  $handler->display->display_options['fields']['floor']['id'] = 'floor';
  $handler->display->display_options['fields']['floor']['table'] = 'ug_whereisit_areas';
  $handler->display->display_options['fields']['floor']['field'] = 'floor';
  $handler->display->display_options['fields']['floor']['separator'] = '';
  /* Field: Area: Code */
  $handler->display->display_options['fields']['code']['id'] = 'code';
  $handler->display->display_options['fields']['code']['table'] = 'ug_whereisit_areas';
  $handler->display->display_options['fields']['code']['field'] = 'code';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Operations';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<a href="/admin/config/whereisit/areas/manage/[id]?destination=admin/config/whereisit/named-areas">edit</a>
<a href="/admin/config/whereisit/named-areas/[id]/clone?destination=admin/config/whereisit/named-areas">clone</a>
<a href="/admin/config/whereisit/areas/manage/[id]/delete?destination=admin/config/whereisit/named-areas">delete</a>
<a href="/admin/config/whereisit/areas/manage/[id]/export">export</a>';
  /* Filter criterion: Area: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'ug_whereisit_areas';
  $handler->display->display_options['filters']['type']['field'] = 'type';

  /* Display: Named Areas */
  $handler = $view->new_display('page', 'Named Areas', 'page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Named Areas';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Area: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'ug_whereisit_areas';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = 'named';
  $handler->display->display_options['path'] = 'admin/config/whereisit/named-areas';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Manage Named Areas';
  $handler->display->display_options['menu']['description'] = 'Manage Named Areas';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Call Number Areas */
  $handler = $view->new_display('page', 'Call Number Areas', 'page_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Call Number Areas';
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['label'] = 'Area Links';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<ul class="action-links"><li><a href="/admin/config/whereisit/areas/add?destination=admin/config/whereisit/call-number-areas">Add area</a></li>
<li><a href="/admin/config/whereisit/areas/import?destination=admin/config/whereisit/call-number-areas">Import area</a></li>
</ul>';
  $handler->display->display_options['header']['area']['format'] = 'filtered_html';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Area: Internal, numeric area ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'ug_whereisit_areas';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['exclude'] = TRUE;
  /* Field: Area: Label */
  $handler->display->display_options['fields']['label']['id'] = 'label';
  $handler->display->display_options['fields']['label']['table'] = 'ug_whereisit_areas';
  $handler->display->display_options['fields']['label']['field'] = 'label';
  $handler->display->display_options['fields']['label']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['label']['alter']['path'] = '/admin/config/whereisit/areas/area/[id]';
  /* Field: Area: Floor */
  $handler->display->display_options['fields']['floor']['id'] = 'floor';
  $handler->display->display_options['fields']['floor']['table'] = 'ug_whereisit_areas';
  $handler->display->display_options['fields']['floor']['field'] = 'floor';
  $handler->display->display_options['fields']['floor']['separator'] = '';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Operations';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<a href="/admin/config/whereisit/areas/manage/[id]?destination=admin/config/whereisit/call-number-areas">edit</a>';
  /* Field: Area: Resource_type */
  $handler->display->display_options['fields']['resource_type']['id'] = 'resource_type';
  $handler->display->display_options['fields']['resource_type']['table'] = 'ug_whereisit_areas';
  $handler->display->display_options['fields']['resource_type']['field'] = 'resource_type';
  /* Field: Area: Start */
  $handler->display->display_options['fields']['start']['id'] = 'start';
  $handler->display->display_options['fields']['start']['table'] = 'ug_whereisit_areas';
  $handler->display->display_options['fields']['start']['field'] = 'start';
  /* Field: Area: End */
  $handler->display->display_options['fields']['end']['id'] = 'end';
  $handler->display->display_options['fields']['end']['table'] = 'ug_whereisit_areas';
  $handler->display->display_options['fields']['end']['field'] = 'end';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Area: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'ug_whereisit_areas';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = 'call';
  $handler->display->display_options['path'] = 'admin/config/whereisit/call-number-areas';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Manage Call Number Areas';
  $handler->display->display_options['menu']['description'] = 'Managed Call Number Areas';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['areas'] = $view;

  return $export;
}
