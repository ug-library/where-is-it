<?php

/**
 * Get all area entities.
 */
function ug_whereisit_get_areas() {
  return ug_whereisit_get_entities('area');
}

/**
 * Get an area by providing its code.
 */
function ug_whereisit_get_area_by_code($code) {
  return ug_whereisit_entity_query('area', array('code' => $code), TRUE);
}

/**
 * Get a floor by providing its code.
 */
function ug_whereisit_get_floor_by_code($code) {
  return ug_whereisit_entity_query('floor', array('code' => $code), TRUE);
}
/**
 * Get all call number areas.
 */
function ug_whereisit_get_call_areas($type) {
  return ug_whereisit_entity_query('area', array('type' => 'call', 'resource_type' => $type));
}

/**
 * Get all floor entities.
 */
function ug_whereisit_get_floors() {
  return ug_whereisit_get_entities('floor');
}

/**
 * Get all material type entities.
 */
function ug_whereisit_get_material_types() {
  $conditions = array('live' => TRUE);
  return ug_whereisit_entity_query('material_type', $conditions);
}

/**
 * Get bounds entities by area ID. If area id is null, just fetch all bounds objects.
 */
function ug_whereisit_get_bounds( $aid = NULL ) {
  //If no area id, just return all entities and be done with it.
  if( is_null($aid)) {
    return ug_whereisit_get_entities('bound', FALSE);
  }
  
  return ug_whereisit_entity_query('bound', array('aid' => $aid));
}

/**
 * Return the path to the image for a floor. 
 * Floors are identified by their 'code' property, not the floor id. 
 */
function ug_whereisit_get_map( $floor_code ) {
  $entity =  ug_whereisit_entity_query('floor', array('code' => $floor_code), TRUE);
  if( !is_null($entity) ) {
    $file_url = file_create_url($entity->image);
    $output = array();
    $output['url'] = $file_url;
    $output['width'] = $entity->image_width;
    $output['height'] = $entity->image_height;

    return $output; 

  }

  return "";
}

/**
 * Get an array of areas, suitable for a select list. 
 * 
 * If floor_code is passed, it will restrict values to a given floor. 
 * Otherwise, all named areas are returned.
 */
function ug_whereisit_get_named_areas_select($floor_code = null) {
  /**
   * See Entity Field Query documentation here:
   * https://www.drupal.org/node/1343708
   */
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'area');
  $query->propertyCondition('type', 'named');

  //If required, restrict to a certain floor.
  if( !is_null($floor_code) ) {
    $query->propertyCondition('floor', $floor_code);
  }
 
  //Sort by label
  $query->propertyOrderBy('label', 'ASC');
  $result = $query->execute();
  $area_ids = array_keys($result['area']);

  /**
   * Entity load returns all results if $area_ids is 'falsy' eg empty. 
   * So, we have to detect that case and return an empty result.
   */
  if( empty($area_ids) ) {
    return array();
  }

  $areas = entity_load('area', $area_ids);
  return ug_whereisit_get_entity_select($areas);
}

/**
 * Given a floor code and an area code, return the corresponding area. 
 */
function ug_whereisit_get_named_area($floor_code, $area_code) {
  return ug_whereisit_entity_query('area', array('floor' => $floor_code, 'code' => $area_code), TRUE); 
}

/**
 * Delete all bounds associated with an area.
 */
function ug_whereisit_delete_bounds( $aid ) {
  $bounds = ug_whereisit_get_bounds($aid);
  foreach($bounds as $bound) {
    entity_delete('bound', $bound->id); 
  }
}

/** 
 * Comparison function for sorting entities with weight fields.
 */
function ug_whereisit_entity_weight_compare( $a, $b ) {
  if( $a->weight == $b->weight ) {
    return 0;
  }
  
  if( $a->weight < $b->weight ) { 
    return -1;
  }
  else {
    return 1;
  }
}

/**
 * Generic function to fetch all entities, ordered by weight.
 *
 * TODO: Potentially refactor this to use EntityFieldQuery so we can sort in the DB rather than in code. 
 */
function ug_whereisit_get_entities($entity_type, $sort = TRUE) {
  $entities = entity_load($entity_type);
  if( $sort ) {
    usort($entities, 'ug_whereisit_entity_weight_compare');
  }
  return $entities;
}

/**
 * Get an array of entities suitable for a form api select list.
 */
function ug_whereisit_get_entity_select($entities) {
  $output = array();
  foreach($entities as $entity) {
      $output[$entity->code] = $entity->label;
  }

  return $output;
}

/**
 * Generic function to query entities of a certain type.
 * 
 * @param string $entity_type The type of the entity. Eg. "area"
 * @param array $conditions An array of conditions for the query as key => value pairs.
 * @param bool $single TRUE for a single result, FALSE for multiple results. 
 */
function ug_whereisit_entity_query($entity_type, $conditions, $single = FALSE) {
  /**
   * See Entity Field Query documentation here:
   * https://www.drupal.org/node/1343708
   */ 
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', $entity_type);
  foreach($conditions as $key => $val) {
    $query->propertyCondition($key, $val);
  }

  $result = $query->execute();

  if(isset($result[$entity_type])) {
    $entity_ids = array_keys($result[$entity_type]);

    if($single) {
     return entity_load_single($entity_type, $entity_ids[0]);
    }
    else {
      return entity_load($entity_type, $entity_ids); 
    }
  }

  if($single) {
    return null;
  }
  else {
    return array();
  }
}

/*
 * Add a default option at the beginning of the options for a select list..
 */
function ug_whereisit_select_add_default( $text, $options_function ) {
  $default = array(null => $text);
  $values = call_user_func($options_function);
  
  $ret = $default + $values;
  return $ret;
}

/**
 * Given a string, create a slug for use in a URL.
 *
 * The pathauto module already has this code, so just use that. 
 */
function ug_whereisit_slug($string) {
  module_load_include('inc', 'pathauto', 'pathauto');
  return pathauto_cleanstring($string);
}

/**
 * Given a list of call numbers, search through a list of areas and return the area which contains
 * the call number. 
 *
 * If no area contains the call number, return NULL.
 */
function ug_whereisit_area_search($areas, $call_number) {
  foreach ($areas as $area) {
    if (ug_whereisit_area_contains_callnumber($area, $call_number)) {
      return $area;
    }     
  }

  return NULL;
}

/**
 * Given a call number, determine if the call number falls inside of an area. 
 */
function ug_whereisit_area_contains_callnumber ($area, $call_number) {
  $start = new CallNumber($area->start);
  $end = new CallNumber($area->end);

  if (compareCallNumber($call_number, $start) >= 1 || compareCallNumber($call_number, $start) == 0 ) { 

    if (compareCallNumber($call_number, $end) <= -1 || compareCallNumber($call_number, $end) == 0) {
      return TRUE;
    }
  }  

  return FALSE;
  
}

/**
 * Given an area, redirect to the public page for that area object.
 */
function ug_whereisit_goto_area($area) {
  if(!is_null($area)) {
    $code = $area->code;
    $path = "/where/areas/area/" . $code;
    drupal_goto($path);
  }
}

/**
 * Given a floor, redirect to the public page for that floor object.
 */
function ug_whereisit_goto_floor($floor) {
  if(!is_null($floor)) {
    $code = $floor->code;
    $path = "/where/floors/floor/" . $code;
    drupal_goto($path);
  }
}
