<?php

function ug_whereisit_call_number_form( $form, &$form_state ) {
  $form = array();
 
  $options = ug_whereisit_get_entity_select(ug_whereisit_get_material_types());
  $default = array_shift(array_keys($options));
  $form['material_type'] = array(
    '#type' => 'radios',
    '#title' => t('Material Type'),
    '#options' => $options,
    '#default_value' => $default,
    '#required' => TRUE,
  );

  $form['call_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Call Number'),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Find it'),
  );

  return $form; 
}

function ug_whereisit_call_number_form_submit( $form, &$form_state ) {
  module_load_include('inc', 'ug_whereisit', 'includes/callnumber');

  $cn = new CallNumber($form_state['values']['call_number']);

  $areas = ug_whereisit_get_call_areas($form_state['values']['material_type']);
  $search_area = ug_whereisit_area_search($areas, $cn);

  if(!is_null($search_area)) {
    $code = $search_area->code;
    $path = "/where/areas/area/" . $code;
    drupal_goto($path);
  }
  else {
    drupal_set_message("No location found.");
  }
}


