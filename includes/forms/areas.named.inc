<?php

function ug_whereisit_named_all_form( $form, &$form_state ) {
  $form = array();
  
  $title = '<span class="element-invisible">' . t('Room or Resource. Note that changing the value of this element will cause the page to reload') . '</span>';
  $form['select'] = array(
    '#type' => 'select',
    '#title' => $title,
    '#options' => ug_whereisit_select_add_default('Choose...', 'ug_whereisit_get_named_areas_select'),
  );

  $form['floors']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go to room'),
    '#attributes' => array(
      'class' => array('element-invisible'),
      'id' => array('where-room-submit'),
    ),  
  );

  return $form; 
}


