<?php

function ug_whereisit_admin() {
  $form = array();

  $form['ug_whereisit_box_colour'] = array(
    '#type' => 'textfield',
    '#title' => t('Box colour'),
    '#default_value' => variable_get('ug_whereisit_box_colour', '000000'),
    '#description' => t('Colour to use for boxes on maps. Enter as a RGB hex value. Use a <a href="https://color.adobe.com/create/color-wheel/?base=2&rule=Monochromatic&selected=0&name=My%20Color%20Theme&mode=rgb&rgbvalues=0.5,0,0,1,0.30000000000000004,0.30000000000000004,1,0,0,0.5,0.15000000000000002,0.15000000000000002,0.8,0,0&swatchOrder=0,1,2,3,4">colour wheel</a> to choose your colour.')
  ); 

  $form['ug_whereisit_line_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Line width'),
    '#default_value' => variable_get('ug_whereisit_line_width', '3'),
    '#description' => t('Line width for boxes. Provide a positive integer value, where higher values mean thicker lines.')
  );

  $form['ug_whereisit_fill_transparency'] = array(
    '#type' => 'textfield',
    '#title' => t('Fill transparency'),
    '#default_value' => variable_get('ug_whereisit_fill_transparency', '0.7'),
    '#description' => t('Transparency for box fill. Enter a number between 0 and 1 where 0 is transparent and 1 is fully opaque. Eg. 0.7')
  );

  return system_settings_form($form); 
}

function ug_whereisit_admin_validate($form, &$form_state) {
  $line_width = $form_state['values']['ug_whereisit_line_width'];

  if(is_numeric($line_width)) {
    $line_width = (int)$line_width;

    if(is_int($line_width) && $line_width < 1) {
      form_set_error('ug_whereisit_line_width', t('Line width must be a positive integer value.'));
    }
  } 
  else {
    form_set_error('ug_whereisit_line_width', t('Line width must be a positive integer value.'));
  }

  $transparency = $form_state['values']['ug_whereisit_fill_transparency'];

  if(is_numeric($transparency)) {

    if($transparency > 1) {
      form_set_error('ug_whereisit_fill_transparency', t('Transparency must be between 0 and 1.'));
    }

    if($transparency < 0) {
      form_set_error('ug_whereisit_fill_transparency', t('Transparency must be between 0 and 1.'));
    }
  } 
  else {
    form_set_error('ug_whereisit_fill_transparency', t('Transparency must be between 0 and 1.'));
  }
}
