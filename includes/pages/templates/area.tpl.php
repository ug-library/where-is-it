<?php
  drupal_add_js(array('ug_whereisit' => array(
    'box_colour' => variable_get('ug_whereisit_box_colour', '000000'),
    'line_width' => variable_get('ug_whereisit_line_width', 3), 
    'fill_transparency' => variable_get('ug_whereisit_fill_transparency', 0.7), 
    'floor' => $element->floor,
    'bounds' => json_encode($element->bounds),
  )), array('type' => 'setting'));
  drupal_add_js(drupal_get_path('module', 'ug_whereisit') . '/js/map.js');
  drupal_add_js(drupal_get_path('module', 'ug_whereisit') . '/js/area-public.js');
  drupal_add_css(drupal_get_path('module', 'ug_whereisit') . '/css/area.css');
?>

<canvas id="map">
  <?php print filter_xss($element->description); ?>
</canvas>
<div id="where-link-wrap"> 
  <a id="where-back-link" class="ug-button" href="/where">Back to Where Is It</a>
</div>
