<?php

function ug_whereisit_area_page_title($area_code) {
  $area = ug_whereisit_get_area_by_code($area_code);
  return $area->label;
}

function ug_whereisit_area_page ($area_code, $view_mode='full') {
  $area = ug_whereisit_get_area_by_code($area_code);

  $content = array(
    '#theme'     => 'area',
    '#element'   => $area,
    '#view_mode' => $view_mode,
    '#language'  => LANGUAGE_NONE,
  );  

  return $content;
}


