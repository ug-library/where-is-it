<?php

function ug_whereisit_floor_page_title($floor_code) {
  $floor = ug_whereisit_get_floor_by_code($floor_code);
  return filter_xss($floor->label);
}

function ug_whereisit_floor_page ($floor_code, $view_mode='full') {
  $floor = ug_whereisit_get_floor_by_code($floor_code);

  $content = array(
    '#theme'     => 'floor',
    '#element'   => $floor,
    '#view_mode' => $view_mode,
    '#language'  => LANGUAGE_NONE,
  );  

  return $content;
}


