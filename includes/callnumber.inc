<?php

Class CallNumber {
  protected $callno;
  protected $parts = array();

  public function __construct($callno) {
    $this->callno = $callno;
    $parser = new CallNumberParser();
    $this->parts = $parser->parse($this->callno);
  }

  public function getParts() {
    return $this->parts;
  }

}

/**
 * Compare two call numbers. 
 * 
 * Returns:
 *   < 1 if $first is less than $second, 
 *   > 1 if $first is greater than $second,
 *   0 if $first = $second  
 */
function compareCallNumber(CallNumber $first, CallNumber $second) {
  $first_parts = $first->getParts();
  $second_parts = $second->getParts();

  $first_size = sizeof($first_parts);
  $second_size = sizeof($second_parts);

  $min = min(array($first_size, $second_size));
  for($index = 0; $index < $min; $index++ ) {
    $first_part = $first_parts[$index]; 
    $second_part = $second_parts[$index]; 
    $result = compareParts($first_part, $second_part);
    if( $result != 0 ) {
      return $result;
    }
  }

  //If we've gotten to this point, all the common parts are the same.
  //At this point, use the length of the call numbers to decide. 
  if( $first_size > $second_size ) {
    return 1;
  }

  if( $first_size < $second_size ) {
    return -1;
  }

  return 0;
}

/**
 * Compare individual parts properly, depending on thier type (string, numeric).
 */
function compareParts($first, $second) {
  if( is_numeric($first) && is_numeric($second) ) {
    if( $first > $second ) {
      return 1;
    }
    else if( $second > $first ) {
      return -1;
    }
    return 0;
  } 

  return strcasecmp($first, $second);
}

/**
 * A call number parser, implemented using a simple regex based finite state machine.
 */
Class CallNumberParser {
  /**
   * These are the transition rules for each state.
   *
   * Rules for each state are formatted as 'regex pattern' => next state.
   */
  protected $rules = array(
    //BS
    0 => array(
      '[A-Za-z]' => 0, 
      '[\s]' => 1,
      '[0-9]' => 1,
    ),
    //BS 2545
    1 => array(
      '[0-9]' => 1,
      '[\s]' => 2,
      '[\.]' => 2,
    ),
    //BS 2545. G
    2 => array(
      '[A-Za-z]' => 2,
      '[0-9]' => 3,
    ),
    //BS 2545. G4
    3 => array(
      '[0-9]' => 3,
      '[A-Za-z]' => 2,
      '[\s]' => 2,
    ),
  );

  protected $parts = array();

  /**
   * Parse a call number string into its constituent parts.
   */
  public function parse($callno) {
    $callno = trim($callno);

    $current = "";
    $symbol = "";

    $state = 0;
    for ($i = 0; $i < strlen($callno); $i++) {
      $symbol = $callno[$i];

      /**
       * Iterate through the rules for our current state, and see if one matches.
       * If one matches we transition, to the next state, which may or may not be a different state than we're currently in.
       * If we're staying in the same state, we add the current symbol to the current part that we're building.
       * If we transition to a different state, store the current part as a component of the call number.
       */
      foreach($this->rules[$state] as $pattern => $next) {

        $result = preg_match("/" . $pattern . "/", $symbol);
        if( $result === 1) {
          //Staying in the same state.
          if($state == $next) {
            $current .= $symbol;
          }

          //Transitioning to another state, so store the current part and initialize the next one.
          if($state != $next) {
            $this->storePart($current);

            /**
             * Starting a new part.
             * We only want parts composed of alphanumeric symbols, so set the initial value of the next part accordingly. 
             */
            if(ctype_alnum($symbol)) {
              $current = $symbol;
            }
            else {
              $current = "";
            } 
          }
          $state = $next;      
        }
      }
    }
    $this->storePart($current);
    return $this->parts;
  }

  protected function storePart($str) {
    if( !empty($str) ) {
      $this->parts[] = $str;
    }
  }
}
